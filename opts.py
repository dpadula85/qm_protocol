#!/usr/bin/env python

import argparse as arg


def options():

    parser = arg.ArgumentParser(
                formatter_class=arg.ArgumentDefaultsHelpFormatter)

    #
    # Input Options
    #
    inp = parser.add_argument_group("Input Data")

    inp.add_argument('--db', required=True, type=str, dest='DBFile',
                     help='''Database File.''')

    inp.add_argument('--qm', required=True, type=str, dest='QMOpts',
                     help='''Quantum Chemistry Options File.''')

    inp.add_argument('-n', '--nconfs', default=1, type=int, dest='NConfs',
                     help='''Number of Conformers for each molecule.''')

    #
    # Output Options
    #
    out = parser.add_argument_group("Output Options")

    out.add_argument('-o', '--outdir', default=None, type=str, dest='OutDir',
                     help='''Output folder.''')

    args = parser.parse_args()
    Opts = vars(args)

    return Opts


if __name__ == '__main__':
    pass
