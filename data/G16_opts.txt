# QM Input Options
program     G16
queue       troisi
time        72:00:00
nproc       40
mem         12
job         opt
funct_opt   b3lyp
funct_es    b3lyp
basis       3-21g
